

const extractIcon = (icon) => {
  if (!icon) { return; }
  let iconClass = icon.split(" ")[0];
  let i = icon.indexOf("-");
  let name = icon.slice(i + 1);
  let tab = [];
  tab.push(iconClass);
  tab.push(name);
  return tab;
};

export { extractIcon};
